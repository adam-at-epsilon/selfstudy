#!/bin/bash

# http://hackaday.com/2017/07/21/linux-fu-better-bash-scripting/

set -o errexit
set -o nounset

function fcn
{
  echo $1
}


echo "before"
echo "A arobas: $@"
echo B arobas: "$@"
echo C arobas: $@

trap "echo GOODBYE!; exit 0" INT TERM EXIT

# ls hubba.txt # script exits due to -o errexit
ls hubba.txt || true # or true allows the script to continue despite fail

echo "TEMP: $TEMP"
# echo "houba: $HOUBA" # -o nounset with unset $HOUBA causes script exit
echo "cmd line param 1: $1" # -o nounset script fails if no arg is given

readonly FOO="fighers"
FOO="bar" # 'readonly' causes script to exit

fcn one two three
fcn "one two three"


echo "testing shift on arobas and star"
echo arobas: $@
echo star: $*
shift
echo arobas: $@
echo star: $*

# "$*" is the only way to get a sentence as a "single string w spaces".

# verdict: equally affected

echo "after"
