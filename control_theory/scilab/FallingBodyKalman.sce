// Reading up on Kalman filters, following ref [0]

// Reference
// [0] http://biorobotics.ri.cmu.edu/papers/sbp_papers/integrated3/kleeman_kalman_basics.pdf
//
// [1] https://courses.cs.washington.edu/courses/cse466/11au/calendar/14-StateEstimation-posted.pdf
// The matrices, their dimensions and what they mean:
// [2] https://www.udacity.com/wiki/cs373/kalman-filter-matrices#!#part-i-who-is-who-in-the-land-of-kalman-filters
// [3] Slide L6.2 of AUTONAVx at TU Munich course.

clc();
clear();
clf();

// ref[0] page 13
// y (t) = y (t0) + ydot(t0)(t - t0) - g/2 * (t - t0)**2
// this is discretised, where t - t0 = 1
// y (k+1) = y(k) + ydot(k)

// n states
xHat = [190; 1]; // [x ; xdot] = [position ; vertical velocity]

// page 15
F = [1 , 1; 0 1]; // this is the process matrix (behaviour of process under zero input)
G = [-0.5;  -1];  // this is the input matrix (behaviour of process from input -g)
H = [1 , 0];   // m = 1 sensors, hence H is an m x n  = 1 x 3 matrix

// hat = estimate

P = [10, 0; 0 1]; // state error covariance chosen "on intuition"
Q = [ 0, 0; 0, 0]; // state noise covariance - all zeros
R = [ 6 ]; // m by m matrix

// force
u = 1;

g = -1; // cool, we're on an alien planet!

rand("normal");
t = 0:50;
[tmp,nSamp] = size(t);


printf("k\txTrue\txDotTrue\tMeasurement\tPosition(est)\tVelocity(est)\tP11\tP22\n");

measHist = 1;
myXMeas = [ 0, 0];
kalmanGain = [ 0 ; 0];
for i = 1:nSamp
    k = t(1,i);
    // calculate true values
    xTrue = 200 + g / 2 * k**2; // we can only observe position with a noisy sensor
    xDotTrue = g * k;
    xMeas = 0;

    // always run estimation    
    xHat = F*xHat + G * u;
    P = F * P * F' + Q;

    if modulo(k - 1, 5) == 0
         // there's a measurement available 
         // so correct the current estimate
        xMeas = xTrue + rand(); // generate a measured value
        myXMeas = [myXMeas(:,:); [k , xMeas]];
        zHat = H * xHat;   // prediction of estimated measurement

        S = H * P * H' + R;
        kalmanGain = P * H' * inv(S); // becomes a 2 x 1 (n x m matrix)
        xHat = xHat + kalmanGain * (xMeas - zHat);
        P = P - kalmanGain * H * P; // ref[3]
        
        measHist = measHist + 1;
    end
    printf("%i\t%3.1f\t%3.1f\t\t%3.1f\t\t%3.1f\t\t%3.1f\t\t%3.1f\t%3.1f\n", k, xTrue, xDotTrue, xMeas, xHat(1,1), xHat(2,1),P(1,1), P(2,2));
    myK(i) = k;
    myXHat(:,i) = xHat;
    myXTrue(:,i) = xTrue;
    myP11(:,i) = P(1,1);
    myP22(:,i) = P(2,2);
    myKG1(:, i) = kalmanGain(1,1);
    myKG2(:, i) = kalmanGain(2,1);

    if xTrue < 0
        break;
    end
end

xgrid(3);
subplot(211);
title("Falling body g:" + string(g) + " m/s2");
xlabel("time kT (T = 1s) [s]")
ylabel("[m]");
plot(myK(:,1), myXTrue(1, :)', 'black-');
plot(myK(:,1), myXHat(1, :)', 'b-');
plot(myXMeas(:,1), myXMeas(:,2), 'ored');
subplot(212);
xgrid(3);
title("P11 - blue, P22 - magenta, kalmanGain(1) - yellow, kalmanGain(2) - cyan");
plot(myK(:, 1), myP11(1,:),'blue');
plot(myK(:, 1), myP22(1,:),'magenta');
plot(myK(:, 1), myKG1(1,:),'yellow');
plot(myK(:, 1), myKG2(1,:),'cyan');
