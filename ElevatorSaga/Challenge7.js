{
    // Challenge #7: Transport 100 people using 63 elevator moves or less
    // sometimes fails to do this, but fails only narrowly.

    init: function(elevators, floors) {
        var upButtons = [0,0,0,0,0,0,0,0,0,0,0,0];
        var downButtons = [0,0,0,0,0,0,0,0,0,0,0,0];

        function sumArray(array) {
            var sum = 0;

            for (var i = 0; i < array.length; i++) {
                sum += array[i];
            }

            return sum;
        }


        function decideFloor(elevator) {
            var maxPresses = 0;
            var nextDest = 0;
            var pressed = elevator.getPressedFloors();


            if (pressed.length > 0) {
                elevator.stop();
                if (elevator.goingUpIndicator() == true) {
                    pressed.sort(function(a, b){return a > b});
                } else if (elevator.goingDownIndicator() == true){
                    pressed.sort(function(a, b){return b < a});
                }

                nextDest = pressed[0];
                elevator.goToFloor(nextDest);

                if (nextDest > elevator.currentFloor()) {
                    elevator.goingDownIndicator(false);
                    elevator.goingUpIndicator(true);

                    upButtons[nextDest] = 0;

                }

                if (nextDest < elevator.currentFloor()) {
                    elevator.goingDownIndicator(true);
                    elevator.goingUpIndicator(false);
                }
            } else {
                elevator.goingDownIndicator(true);
                elevator.goingUpIndicator(true);

                if (sumArray(upButtons) >= sumArray(downButtons)) {
                    for (var i = 0; i < floors.length; i++) {
                        if (upButtons[i] > maxPresses) {
                            nextDest = i;
                            break;
                        }
                    }
                    upButtons[nextDest] = 0;
                } else {
                    for (var i = 0; i < floors.length; i++) {
                        if (downButtons[floors.length - 1 - i] > maxPresses) {
                            nextDest = floors.length - 1 - i;
                            break;
                        }
                    }
                    downButtons[nextDest] = 0;
                }
                elevator.goToFloor(nextDest);
            }
        }


        _.each(elevators, function(elevator) {

            // Whenever the elevator is idle (has no more queued destinations) ...
            elevator.on("idle", function() {
                decideFloor(elevator);
            });

            elevator.on("floor_button_pressed", function(floorNum) {
                decideFloor(elevator);
            });

            elevator.on("stopped_at_floor", function(floorNum) {
                decideFloor(elevator);
            });

            elevator.on("passing_floor", function(floorNum, direction) {
                var stopHere = 0;
                var elevRestCapacity = Math.ceil((1 - elevator.loadFactor()) * elevator.maxPassengerCount());

                if ((direction == "up") &&
                    (true == elevator.goingUpIndicator()) &&
                    (upButtons[floorNum] >= 1) &&
                    (elevRestCapacity >= 2)) {
                    upButtons[floorNum] = 0;
                    stopHere = 1;
                }

                if ((direction == "down") &&
                    (true == elevator.goingDownIndicator()) &&
                    (downButtons[floorNum] >= 1) &&
                    (elevRestCapacity >= 2)) {
                    downButtons[floorNum] = 0;
                    stopHere = 1;
                }

                if (stopHere != 0) {
                    elevator.destinationQueue.unshift(floorNum);
                    elevator.checkDestinationQueue();
                }
            });
        });

        _.each(floors, function(floor) {
            floor.on("up_button_pressed", function() {
                upButtons[floor.floorNum()] += 1;

                for (var i = 0; i < elevators.length; i++) {
                    if (("stopped" == elevators[i].destinationDirection()) &&
                        (elevators[i].loadFactor() == 0)) {
                        decideFloor(elevators[i]);
                        break;
                    }
                }
            });

            floor.on("down_button_pressed", function() {
                downButtons[floor.floorNum()] += 1;

                for (var i = 0; i < elevators.length; i++) {
                    var rev_idx = elevators.length - 1 - i;
                    if (("stopped" == elevators[rev_idx].destinationDirection()) &&
                        (elevators[rev_idx].loadFactor() == 0)) {
                        decideFloor(elevators[rev_idx]);
                        break;
                    }
                }
            });

        });

    },

    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}
