{
    init: function(elevators, floors) {
        var upButtons = [0,0,0,0,0,0,0,0];
        var downButtons = [0,0,0,0,0,0,0,0];

        function checkPressedFloors(elevator, defaultFloor) {
            if (elevator.loadFactor() == 0) {
                var i = 0;
                if (elevator.goingUpIndicator() == true) {

                    for (i = 0; i < upButtons.length; i++) {
                        if (upButtons[i] == 1) {
                            elevator.goToFloor(i);
                            upButtons[i] = 0;
                            return;
                        }
                    }

                    elevator.goToFloor(0);
                } else {
                    for (i = 0; i < upButtons.length; i++) {
                        if (downButtons[downButtons.length - 1 - i] == 1) {
                            elevator.goToFloor(downButtons.length - 1 - i);
                            downButtons[downButtons.length - 1 - i] = 0;
                            return;
                        }
                    }
                    elevator.goToFloor(7);
                }


            } else {
                var pressed = elevator.getPressedFloors();
                if (pressed.length > 0) {
                    elevator.stop();
                    if (elevator.goingUpIndicator() == true) {
                        pressed.sort(function(a, b){return a > b});
                    } else {
                        pressed.sort(function(a, b){return b < a});
                    }

                    var nextDest = pressed[0];
                    elevator.goToFloor(nextDest);
                }
            }
        }

        elevators[0].on("idle", function() {
            elevators[0].goingUpIndicator(true);
            elevators[0].goingDownIndicator(false);
            checkPressedFloors(elevators[0], 0);
        });

        elevators[1].on("idle", function() {
            elevators[1].goingUpIndicator(false);
            elevators[1].goingDownIndicator(true);
            checkPressedFloors(elevators[1], 7);
        });

        elevators[0].on("passing_floor", function(floorNum, direction) {
            if (direction == "up") {
                if (upButtons[floorNum] == 1) {
                    if (elevators[0].loadFactor() < 1) {
                        elevators[0].destinationQueue.unshift(floorNum);
                        elevators[0].checkDestinationQueue();
                    }
                }
            }
        });

        elevators[1].on("passing_floor", function(floorNum, direction) {
            if (direction == "down") {
                if (downButtons[floorNum] == 1) {
                    if (elevators[1].loadFactor() < 0.8) {
                        elevators[1].destinationQueue.unshift(floorNum);
                        elevators[1].checkDestinationQueue();
                    }
                }
            }
        });
        elevators[0].on("stopped_at_floor", function(floorNum) {
            // Maybe decide where to go next?
            upButtons[floorNum] = 0;
        });


        elevators[1].on("stopped_at_floor", function(floorNum) {
            // Maybe decide where to go next?
            downButtons[floorNum] = 0;
        });


        elevators[0].on("floor_button_pressed", function(floorNum) {
            checkPressedFloors(elevators[0], 0);
        });

        elevators[1].on("floor_button_pressed", function(floorNum) {
            checkPressedFloors(elevators[1], 7);
        });


        _.each(floors, function(floor) {
            floor.on("up_button_pressed", function() {
                upButtons[floor.floorNum()] = 1;

                if (elevators[0].loadFactor() == 0) {
                    elevators[0].goToFloor(floor.floorNum());
                }
            });

            floor.on("down_button_pressed", function() {
                downButtons[floor.floorNum()] = 1;

                if (elevators[1].loadFactor() == 0) {
                    elevators[1].goToFloor(floor.floorNum());
                }
            });
        });
    },
    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}
