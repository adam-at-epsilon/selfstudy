{
    // Challenge #8: Transport 50 people and let no one wait more than 21.0 seconds

    // this fails because the passing_floor heuristics don't stop the elevator when someone's waiting.

    // how about using the load factor to disallow the elevator changing direction while
    // it's loaded?
    init: function(elevators, floors) {
        var upButtons = [0,0,0,0,0,0,0,0,0,0,0,0];
        var downButtons = [0,0,0,0,0,0,0,0,0,0,0,0];

        function sumArray(array) {
            var sum = 0;

            for (var i = 0; i < array.length; i++) {
                sum += array[i];
            }

            return sum;
        }


        function decideFloor(elevator) {
            var nextDest = 0;
            var pressed = elevator.getPressedFloors();

            if (pressed.length > 0) {
                // choose a destination from the button presses.
                elevator.stop();
                if (elevator.goingUpIndicator() == true) {
                    pressed.sort(function(a, b){return a > b});
                } else if (elevator.goingDownIndicator() == true){
                    pressed.sort(function(a, b){return b < a});
                }

                nextDest = pressed[0];

                if (nextDest > elevator.currentFloor()) {
                    elevator.goingDownIndicator(false);
                    elevator.goingUpIndicator(true);
                }

                if (nextDest < elevator.currentFloor()) {
                    elevator.goingDownIndicator(true);
                    elevator.goingUpIndicator(false);
                }
            } else {
                // choose a destination from up/down button presses
                elevator.goingUpIndicator(true);
                elevator.goingDownIndicator(true);

                if (sumArray(upButtons) >= sumArray(downButtons)) {
                    for (var i = 0; i < floors.length; i++) {
                        if (upButtons[i] > 0) {
                            nextDest = i;
                            upButtons[nextDest] = 0;
                            break;
                        }
                    }
                } else {
                    for (var i = 0; i < floors.length; i++) {
                        if (downButtons[floors.length - 1 - i] > 0) {
                            nextDest = floors.length - 1 - i;
                            downButtons[nextDest] = 0;
                            break;
                        }
                    }
                }
            }
            elevator.goToFloor(nextDest);
        }


        _.each(elevators, function(elevator) {

            // Whenever the elevator is idle (has no more queued destinations) ...
            elevator.on("idle", function() {
                decideFloor(elevator);
            });

            elevator.on("floor_button_pressed", function(floorNum) {
                decideFloor(elevator);
            });

            elevator.on("stopped_at_floor", function(floorNum) {
                decideFloor(elevator);
            });

            elevator.on("passing_floor", function(floorNum, direction) {
                var stopHere = 0;
                var elevRestCapacity = Math.ceil((1 - elevator.loadFactor()) * elevator.maxPassengerCount());

                if ((direction == "up") &&
                    (true == elevator.goingUpIndicator()) &&
                    (upButtons[floorNum] >= 1)) {
                    upButtons[floorNum] = 0;
                    stopHere = 1;
                }

                if ((direction == "down") &&
                    (true == elevator.goingDownIndicator()) &&
                    (downButtons[floorNum] >= 1)) {
                    downButtons[floorNum] = 0;
                    stopHere = 1;
                }

                if (stopHere != 0) {
                    elevator.destinationQueue.unshift(floorNum);
                    elevator.checkDestinationQueue();
                }
            });
        });

        _.each(floors, function(floor) {
            floor.on("up_button_pressed", function() {
                upButtons[floor.floorNum()] += 1;

                for (var i = 0; i < elevators.length; i++) {
                    if (("stopped" == elevators[i].destinationDirection()) &&
                        (elevators[i].loadFactor() == 0)) {
                        decideFloor(elevators[i]);
                        return;
                    }
                }
            });

            floor.on("down_button_pressed", function() {
                downButtons[floor.floorNum()] += 1;

                for (var i = 0; i < elevators.length; i++) {
                    var rev_idx = elevators.length - 1 - i;
                    if (("stopped" == elevators[rev_idx].destinationDirection()) &&
                        (elevators[rev_idx].loadFactor() == 0)) {
                        decideFloor(elevators[rev_idx]);
                        return;
                    }
                }
            });

        });

    },

    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}
