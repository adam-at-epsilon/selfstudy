{

    init: function(elevators, floors) {
        var upButtons = [0,0,0,0,0,0,0,0,0,0,0,0];
        var downButtons = [0,0,0,0,0,0,0,0,0,0,0,0];

        function sumArray(array) {
            var sum = 0;

            for (var i = 0; i < array.length; i++) {
                sum += array[i];
            }

            return sum;
        }


        function decideFloor(elevator) {

            if (elevator.loadFactor() == 0) {
                var maxPresses = 0;
                var maxIdx = 0;

                if (sumArray(upButtons) >= sumArray(downButtons)) {
                    for (var i = 0; i < floors.length; i++) {
                        if (upButtons[i] > maxPresses) {
                            maxIdx = i;
                            // maxPresses = upButtons[i];
                            break;
                        }
                    }
                    upButtons[maxIdx] -= Math.min(elevator.maxPassengerCount(), upButtons[maxIdx]) ;
                } else {
                    for (var i = 0; i < floors.length; i++) {
                        if (downButtons[floors.length - 1 - i] > maxPresses) {
                            maxIdx = floors.length - 1 - i;
                            // maxPresses = downButtons[i];
                            break;
                        }
                    }
                    downButtons[maxIdx] -= Math.min(elevator.maxPassengerCount(), downButtons[maxIdx]) ;;
                }
                elevator.goToFloor(maxIdx);
            } else {
                var pressed = elevator.getPressedFloors();
                if (pressed.length > 0) {
                    elevator.stop();
                    if (elevator.goingUpIndicator() == true) {
                        pressed.sort(function(a, b){return a > b});
                    } else if (elevator.goingDownIndicator() == true){
                        pressed.sort(function(a, b){return b < a});
                    }

                    var nextDest = pressed[0];
                    elevator.goToFloor(nextDest);

                    if (nextDest > elevator.currentFloor()) {
                        elevator.goingDownIndicator(false);
                        elevator.goingUpIndicator(true);
                    }

                    if (nextDest < elevator.currentFloor()) {
                        elevator.goingDownIndicator(true);
                        elevator.goingUpIndicator(false);
                    }
                } else {
                    elevator.goingDownIndicator(true);
                    elevator.goingUpIndicator(true);
                }
            }
        }


        _.each(elevators, function(elevator) {

            // Whenever the elevator is idle (has no more queued destinations) ...
            elevator.on("idle", function() {
                decideFloor(elevator);
            });

            elevator.on("floor_button_pressed", function(floorNum) {
                decideFloor(elevator);
            });

            elevator.on("stopped_at_floor", function(floorNum) {
                decideFloor(elevator);
            });

            elevator.on("passing_floor", function(floorNum, direction) {
                var stopHere = 0;
                var elevRestCapacity = (1 - elevator.loadFactor()) * elevator.maxPassengerCount();

                if ((direction == "up") &&
                    (true == elevator.goingUpIndicator()) &&
                    (upButtons[floorNum] != 0) &&
                    (elevRestCapacity >= upButtons[floorNum])) {
                    upButtons[floorNum] = 0;
                    stopHere = 1;
                }

                if ((direction == "down") &&
                    (true == elevator.goingDownIndicator()) &&
                    (0 != downButtons[floorNum]) &&
                    (elevRestCapacity >= downButtons[floorNum])) {
                    downButtons[floorNum] = 0;
                    stopHere = 1;
                }

                if (stopHere != 0) {
                    elevator.destinationQueue.unshift(floorNum);
                    elevator.checkDestinationQueue();
                }
            });
        });

        _.each(floors, function(floor) {
            floor.on("up_button_pressed", function() {
                upButtons[floor.floorNum()] += 1;

                for (var i = 0; i < elevators.length; i++) {
                    if (("stopped" == elevators[i].destinationDirection()) &&
                        (elevators[i].loadFactor() == 0)) {
                        decideFloor(elevators[i]);
                        break;
                    }
                }
            });

            floor.on("down_button_pressed", function() {
                downButtons[floor.floorNum()] += 1;

                for (var i = 0; i < elevators.length; i++) {
                    var rev_idx = elevators.length - 1 - i;
                    if (("stopped" == elevators[rev_idx].destinationDirection()) &&
                        (elevators[rev_idx].loadFactor() == 0)) {
                        decideFloor(elevators[rev_idx]);
                        break;
                    }
                }
            });

        });

    },

    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}
