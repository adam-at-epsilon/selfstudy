// Challenge 2 & 4: transport 15 & 23 people in less than 60s - done

{
    init: function(elevators, floors) {
        var elevator = elevators[0]; // Let's use the first elevator

        elevator.on("idle", function() {
            // let's go to all the floors (or did we forget one?)
            elevator.goToFloor(0);
        });

        elevator.on("floor_button_pressed", function(floorNum) {
            // floor nbr button inside elevator pressed
            elevator.goToFloor(floorNum);
        });

        elevator.on("stopped_at_floor", function(floorNum) {
            // Maybe decide where to go next?
            if (elevator.currentFloor() == 0) {
                elevator.goingUpIndicator(true);
                elevator.goingDownIndicator(false);
            } else if (elevator.currentFloor() == (floors.length - 1)) {
                elevator.goingUpIndicator(false);
                elevator.goingDownIndicator(true);
            } else {
                elevator.goingUpIndicator(true);
                elevator.goingDownIndicator(true);
            }
        })

    },
    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
    }
}
