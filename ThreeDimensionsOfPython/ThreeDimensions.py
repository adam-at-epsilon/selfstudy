# import matplot3Dbased.wiresphere as ws
# import matplot3Dbased.anim3d as a3d
import matplot2Dbased.anim2d as a2d

import time
from data_source import random_gen

# ws.sphere()
# a3d.anim3d()

a2d.run2dPlot()

new_data = random_gen()
for i in xrange(40):
    a2d.add_plot_data(next(new_data))
    time.sleep(0.1)
    
