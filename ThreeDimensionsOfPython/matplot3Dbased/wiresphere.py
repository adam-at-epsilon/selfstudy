'''
Created on 12 nov. 2015

@author: adam.at.epsilon@gmail.com
'''

import matplotlib as mpl
# from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import axes3d
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation

fig = plt.figure()
ax = fig.gca(projection='3d')
line = ax.plot([], [], [], label='parametric curve')

def init():
    line.set_data([], [], [])
    return line

# centrePoint is xyz numpy array
def sphere():
    mpl.rcParams['legend.fontsize'] = 10

    theta = np.linspace(-4 * np.pi, 4 * np.pi, 100)
    z = np.linspace(-2, 2, 100)
    r = z ** 2 + 1
    x = r * np.sin(theta)
    y = r * np.cos(theta)
    ax.plot(x, y, z, label='parametric curve')
    ax.legend()

    plt.show()


    
